app_path=../../application

# build maven jar

mvn clean package -f $app_path/pom.xml
cp $app_path/target/*.jar .

# build image
docker build --rm -t softweng/spring-hw .

# cleanup
rm *.jar