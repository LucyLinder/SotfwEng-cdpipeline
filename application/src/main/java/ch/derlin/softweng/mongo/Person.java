package ch.derlin.softweng.mongo;

import org.springframework.data.annotation.Id;

/**
 * date: 12.06.18
 *
 * @author Lucy Linder <lucy.derlin@gmail.com>
 */
public class Person {

    @Id
    private String id;

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}