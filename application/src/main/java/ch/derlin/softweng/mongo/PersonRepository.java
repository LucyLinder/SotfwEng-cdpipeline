package ch.derlin.softweng.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * date: 12.06.18
 *
 * @author Lucy Linder <lucy.derlin@gmail.com>
 */
@RepositoryRestResource(collectionResourceRel = "people", path = "people")
public interface PersonRepository extends MongoRepository<Person, String> {
    // Because you defined it to return List<Person> in the code, it will return all of the results.
    // If you had defined it only return Person, it will pick one of the Person objects to return.
    // Since this can be unpredictable, you probably don’t want to do that for queries that can return multiple entries.
    //
    // You can also issue PUT, PATCH, and DELETE REST calls to either replace, update, or delete existing records.
    List<Person> findByLastName(@Param("name") String name);

}