package ch.derlin.softweng.hw;

/**
 * date: 12.06.18
 *
 * @author Lucy Linder <lucy.derlin@gmail.com>
 */
public class Greeting {
    private final long id;
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
