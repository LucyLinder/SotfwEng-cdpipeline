## pipeline and docker 

See the [OpenAffect Server repo](https://github.com/openaffect/openaffect-server) for more info.

## Spring boot application

resources: 
* [getting started](https://spring.io/guides/gs/rest-service/)
* [using MongoDB](https://spring.io/guides/gs/accessing-mongodb-data-rest/)


# Troubles 

__missing versions__

If it does not compile/run properly try:
```
mvn spring-boot:run --debug
```

__no manifest in jar__

add the lines:
```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <version>1.2.5.RELEASE</version>
    <!-- add the below lines ! -->
    <executions>
        <execution>
            <goals>
                <goal>repackage</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

see [here](https://stackoverflow.com/a/32344823/2667536).

## Running

```bash 
git clone https://gitlab.com/LucyLinder/SotfwEng-cdpipeline
cd docker-topologies/cdpipeline
docker-compose up
```

Now, navigate to [http://localhost:1080](http://localhost:1080) and press `build`. You will see that jenkins will:

- clone the latest version of this repo
- build a new docker image for the application
- launch the app+mongodb using the runtime docker-compose topology.

